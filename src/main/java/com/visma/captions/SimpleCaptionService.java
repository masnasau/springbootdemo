package com.visma.captions;

import org.springframework.stereotype.Service;

@Service
public class SimpleCaptionService implements CaptionService {
    @Override
    public String getCaption(String key) {
        return "Simple caption service";
    }
}
