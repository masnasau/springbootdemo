package com.visma.captions;

public interface CaptionService {
    String getCaption(final String key);
}
