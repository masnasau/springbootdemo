package com.visma.demo;

import com.visma.captions.CaptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component
public class StartPage implements CommandLineRunner {

    @Autowired
    private CaptionService captionService;

    @Override
    public void run(String... strings) throws Exception {
        System.out.println(start());
        final Scanner scanner = new Scanner(System.in);
        final String input = scanner.next();
        System.out.println("Your input was " + input);
    }

    private String start() {
        return captionService.getCaption("a");
    }
}
