package com.visma.config;

import com.google.common.base.Objects;
import com.visma.captions.CaptionService;
import com.visma.captions.FileCaptionService;
import com.visma.captions.SimpleCaptionService;
import com.visma.config.properties.CaptionProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationConfig {

    @Autowired
    private CaptionProperties captionProperties;

    @Bean
    public CaptionService captionService() {
        final CaptionType type = CaptionType.valueOf(captionProperties.getType());

        init();

        if (Objects.equal(type, CaptionType.FILE)) {
            return new FileCaptionService();
        }

        if (Objects.equal(type, CaptionType.SIMPLE)) {
            return new SimpleCaptionService();
        }

        return new SimpleCaptionService();
    }

    private void init() {
        System.out.println("creating bean CaptionService");
    }

    private enum CaptionType {
        FILE,
        SIMPLE
    }
}
